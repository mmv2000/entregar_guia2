#!usr/bin/env python3

# variables.
dic = {'Histidine': 'C6H9N3O2'}
dic_nuevo = {}
lista = []
newlist = []


# funcion cambio: transforma values de dic
# en una lista de tamaño = 8.
def cambio(lista, dic):
    for key, val in dic.items():
        for x in val:
            lista.append(x)


# funcion juntar_de_2: junta de 2 en 2 los elementos de lista
# y se agregan a la nueva lista (newlist)
# agregando a cada par de caracter el objeto "Histidine".
def juntar_de_2(lista, newlist):
    for i in range(len(lista)):
        if i % 2 != 0:
            pass
        else:
            newlist.append(lista[i] + lista[(i + 1)])
    for i in newlist:
        dic_nuevo[i] = 'Histidine'


# main: se llama a cada funcion
# imprime el diccionario nuevo.
if __name__ == '__main__':
    cambio(lista, dic)
    juntar_de_2(lista, newlist)
    print('DICCIONARIO:', dic_nuevo)
